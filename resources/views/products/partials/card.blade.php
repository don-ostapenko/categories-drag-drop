<div class="row">
@foreach($products as $product)
        <div class="col-sm-4 col-lg-4 mb-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $product->name }}</h5>
                    <p class="card-text">{{ $product->price }}</p>
                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit</a>
                </div>
            </div>
        </div>
@endforeach
</div>
