@extends('layouts.app')

@section('content')
    <h1 class="mt-3 mb-3">Categories</h1>
    @include('categories.partials.tree')
    <a href="{{ route('categories.create') }}" class="btn btn-md btn-secondary mt-4">Add category</a>
@endsection
