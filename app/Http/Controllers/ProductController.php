<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Category  $category
     *
     * @return \Illuminate\View\View
     */
    public function index(Category $category)
    {
        $products = Product::whereCategory_id($category->id)->get();
        return view('products.index', compact('products', 'category'));
    }

    /**
     * Show form for create.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryService->getCategoriesTree();
        $level = 0;
        return view('products.create', compact('categories', 'level'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $params = $request->all();
        Category::findOrFail($params['category_id']);
        Product::create($params);
        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::with('category')->findOrFail($id);
        $categories = $this->categoryService->getCategoriesTree();
        $level = 0;
        return view('products.edit', compact('categories', 'level', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProductRequest  $request
     * @param  \App\Models\Product  $product
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, Product $product)
    {
        $params = $request->all();
        $product->update($params);
        return redirect()->route('categories.index');
    }

}
