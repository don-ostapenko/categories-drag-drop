@foreach($childrenCategories as $child)
    <li class="list-group-item @if($child->children->first()) hasItems @endif" data-id="{{ $child->id }}">
        <a href="{{ route('categories.edit', $child->id) }}" class="mr-2" data-toggle="tooltip" data-placement="bottom" title="Edit category">Edit</a>
        <a href="{{ route('products.index', $child->id) }}" class="mr-2" data-toggle="tooltip" data-placement="bottom" title="Show products by category">{{ $child->name }}</a>
        @if($child->children->first())
            <ul class="sublist">
                @include('categories.partials.child', ['childrenCategories' => $child->children])
            </ul>
        @endif
    </li>
@endforeach
