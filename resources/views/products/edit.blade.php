@extends('layouts.app')

@section('content')
    <div class="site-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-5">
                    <div><h2 class="text-black mb-4 mt-3">Add new product</h2></div>
                    <div>
                        <form action="{{ route('products.update', $product) }}" class="text-left" method="post">
                            @csrf
                            @method('put')

                            <div class="form-group">
                                <label for="name">Name <small class="text-danger">*</small></label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                       id="name"
                                       min="3"
                                       max="255"
                                       value="{{ old('name', isset($product) ? $product->name : null) }}" required autofocus>
                                @include('layouts.input-error', ['field' => 'name'])
                            </div>

                            <div class="form-group">
                                <label for="price">Price <small class="text-danger">*</small></label>
                                <input type="number" name="price" class="form-control @error('price') is-invalid @enderror"
                                       id="price"
                                       min="0"
                                       value="{{ old('price', isset($product) ? $product->price : null) }}" required autofocus>
                                @include('layouts.input-error', ['field' => 'price'])
                            </div>

                            <div class="form-group">
                                <label for="parent_id">Category <small class="text-danger">*</small></label>
                                <select name="category_id" id="category_id" class="form-control @error('category_id') is-invalid @enderror" required>
                                    <option value="{{ $product->category->id }}" selected>{{ $product->category->name }}</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @if($category->children->first())
                                            @include('categories.partials.child-option', ['childrenCategories' => $category->children, 'level' => $level + 1])
                                        @endif
                                    @endforeach
                                </select>
                                @include('layouts.input-error', ['field' => 'category_id'])
                            </div>

                            <div class="mb-4">
                                <button type="submit" class="btn btn-md btn-primary mr-2">Update</button>
                                <a href="{{ route('categories.index') }}" class="btn btn-md btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
