## About application

This is a simple app, that include the hierarchical tree of categories (with drag-&-drop sorting).

Steps of deployment

- Clone the project.
- Run `docker-compose up -d` in root project.
- Run `docker-compose run --rm php composer install`.
- Run `docker-compose run --rm php php artisan migrate --seed`.
