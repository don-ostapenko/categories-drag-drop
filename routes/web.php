<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('categories.index');
});


Route::post('/categories/update-sort-category', 'CategoryController@updateSortCategory')->name('categories.updateSortCategory');
Route::resource('products', 'ProductController');
Route::get('/products/list/{category}', 'ProductController@index')->name('products.index');
Route::resource('categories', 'CategoryController');
