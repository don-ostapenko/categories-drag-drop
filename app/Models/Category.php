<?php

namespace App\Models;

use App\Traits\InsertOnDuplicateKey;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use InsertOnDuplicateKey;

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = ['parent_id', 'name', 'sort_category'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Scope for sort by params.
     *
     * @param $query
     * @param  array  $sort
     *
     * @return mixed
     */
    public function scopeOfSort($query, array $sort)
    {
        foreach ($sort as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }

    /**
     * Make categories tree from collection.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $categories
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function makeTree(Collection $categories)
    {
        $grouped = $categories->groupBy('parent_id');

        foreach ($categories as $category) {
            if ($grouped->has($category->id)) {
                $category->children = $grouped[$category->id];
            }
        }

        return $categories->where('parent_id', 0);
    }

}
