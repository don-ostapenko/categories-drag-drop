<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->categoryService->getCategoriesTree();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show form for create.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categoryService->getCategoriesTree();
        $level = 0;
        return view('categories.create', compact('categories', 'level'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        $params = $request->all();
        $this->categoryService->saveCategory($params);
        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $categories = $this->categoryService->getSortedCategories();
        $category = $this->categoryService->findCategory($categories, $id);
        $categories = Category::makeTree($categories);
        $level = 0;
        return view('categories.edit', compact('categories', 'level', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     * @param  \App\Models\Category  $category
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $params = $request->all();
        $this->categoryService->updateCategory($params, $category);
        return redirect()->route('categories.index');
    }

    /**
     * Update sort category.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function updateSortCategory(Request $request)
    {
        $params = $request->all()['data'];
        $this->categoryService->updateOrder($params);
    }

}
