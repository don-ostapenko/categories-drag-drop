@foreach($childrenCategories as $child)
    {{ $level }}
    <option value="{{ $child->id }}">{{ str_repeat('--', $level) . ' ' . $child->name }}</option>
    @if($child->children->first())
        @include('categories.partials.child-option', ['childrenCategories' => $child->children, 'level' => $level + 1])
    @endif
@endforeach
