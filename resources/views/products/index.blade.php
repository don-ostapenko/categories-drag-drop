@extends('layouts.app')

@section('content')
    <h1 class="mt-3 mb-3">{{ $category->name }}</h1>
    @include('products.partials.card')
    <a href="{{ route('products.create') }}" class="btn btn-md btn-secondary mt-4">Add product</a>
@endsection
