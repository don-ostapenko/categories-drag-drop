<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Database\Eloquent\Collection;

class CategoryService
{

    /**
     * Return tree for print into the view.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getCategoriesTree()
    {
        $categories = $this->getSortedCategories();
        return Category::makeTree($categories);
    }

    /**
     * Save category into the DB.
     *
     * @param  array  $params
     */
    public function saveCategory(array $params)
    {
        $params = $this->getDataForDb($params);
        Category::create($params);
    }

    /**
     * Update category into the DB.
     *
     * @param  array  $params
     * @param  \App\Models\Category  $category
     */
    public function updateCategory(array $params, Category $category)
    {
        $params = $this->getDataForDb($params);
        $category->update($params);
    }

    /**
     * Update sort_category by insertOnDuplicateKey method.
     *
     * @param  array  $params
     */
    public function updateOrder(array $params)
    {
        $i = 0;

        foreach ($params as $category) {
            $data[$i]['id'] = $category['id'];
            $data[$i]['name'] = '';
            $data[$i]['sort_category'] = $category['sort_category'];
            $i++;
        }

        Category::insertOnDuplicateKey($data, ['sort_category']);
    }

    /**
     * Sort categories by params.
     *
     * @return mixed
     */
    public function getSortedCategories()
    {
        return Category::OfSort(['parent_id' => 'asc', 'sort_category' => 'asc',])
            ->with('children', 'parent')
            ->get();
    }

    /**
     * Get category from collection.
     *
     * @param  \Illuminate\Database\Eloquent\Collection  $categories
     * @param  int  $id
     *
     * @return mixed
     */
    public function findCategory(Collection $categories, int $id)
    {
        return $categories->first(function ($value) use ($id) {
            if ($value->id == $id) {
                return $value;
            }
        });
    }

    /**
     * Prepare data for DB.
     *
     * @param  array  $params
     *
     * @return mixed
     */
    private function getDataForDb(array $params)
    {
        $sortCategory = Category::whereParent_id($params['parent_id'])
            ->max('sort_category');

        if ($sortCategory && $sortCategory > 0) {
            $params['sort_category'] = $sortCategory + 1;
        }

        return $params;
    }

}
