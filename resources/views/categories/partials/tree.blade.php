<ul id="list1" class='mainlist m-0 p-0'>
    @foreach($categories as $category)
        <li class="list-group-item @if($category->children->first()) hasItems @endif" data-id="{{ $category->id }}">
            <a href="{{ route('categories.edit', $category->id) }}" class="mr-2" data-toggle="tooltip" data-placement="bottom" title="Edit category">Edit</a>
            <a href="{{ route('products.index', $category->id) }}" class="mr-2" data-toggle="tooltip" data-placement="bottom" title="Show products by category">{{ $category->name }}</a>
            @if($category->children->first())
                <ul class="sublist">
                    @include('categories.partials.child', ['childrenCategories' => $category->children])
                </ul>
            @endif
        </li>
    @endforeach
</ul>
