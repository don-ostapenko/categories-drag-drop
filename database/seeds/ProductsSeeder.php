<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Generator as Faker;

class ProductsSeeder extends Seeder
{

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * ProductsTableSeeder constructor.
     *
     * @param  \Faker\Generator  $faker
     */
    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'category_id' => 1,
                'name' => 'Apple Iphone 11 Pro 64GB Black',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 1,
                'name' => 'Apple iPad Pro 11 64GB Cellular',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 2,
                'name' => 'Apple MacBook Pro 13.3 Space Grey',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 3,
                'name' => 'Apple Watch Series 3 38mm Space Grey',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 3,
                'name' => 'Apple Watch Series 3 38mm Space Grey',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 4,
                'name' => 'Garden swing AMF Zen',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 5,
                'name' => 'Hammock AMF Mexico',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 6,
                'name' => 'Plastic table AMF Urano',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 7,
                'name' => 'Barbecue grill Time eco 22018b',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 7,
                'name' => 'Set of dishes Rondell RDS-040 Flamme 8 itm.',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 8,
                'name' => 'Set of pans Tefal Ingenio Elegance 3 itm.',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 8,
                'name' => 'Metal teapot Rondell RDS-498 Fiero 3 ltr.',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'category_id' => 8,
                'name' => 'French press Maxmark MK-F65-1000 1 ltr.',
                'price' => $this->faker->numberBetween(250, 36000),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ];

        DB::table('products')->insert($products);
    }

}
