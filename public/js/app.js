$(document).ready(function () {

    // Sort for parent and child categories
    $('ul.mainlist').sortable({
        connectWith: 'ul.mainlist',
    })

    $('ul').sortable({
        cursor: "move",
        update: function(event, ui) {
            let categoryId = ui.item[0].dataset.id;
            let items = {}
            let ul = $("li[data-id='" + categoryId + "']").parent();
            ul.children().each(function (index, val) {
                items[index] = {
                    id: this.getAttribute('data-id'),
                    sort_category: index
                }
            })
            updateSortCategory(items)
            showPopup()
        }
    });


    // Update sort categories
    function updateSortCategory(items) {
        $.ajax({
            type: "post",
            url: "/categories/update-sort-category",
            data: {
                "data": items
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (response) {
            return response;
        });
        return false;
    }


    // Show popup
    function showPopup() {
        let data = {
            text: "Category was successfully updated!",
            icon: "success",
            title: "Success!",
            button: "Aww yiss!"
        }
        swal(data);
    }
});
